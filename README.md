# AMR_het

This is an adaptation of the code written by Adam Zienkiewicz. The original code is an individual base model of the epidemiological model for the dynamics of two strains of gonorrhea in the context of MSM (Men having sex with men). In this adaptation, the case of two genders is considered.