%mi input es el objeto entero
adj=model.adj_full;
g=graph(adj);
h=plot(g,'EdgeColor','k');
%marker size
ms=8;



%EDGES (all black)
%highlight(h,g.Edges.EndNodes,'EdgeColor','k','LineWidth',1); %no tengo
%idea como hacer esto por el momento
%NODES
%females with diamonds
index1=find(model.GENDER==1);%fems
highlight(h,index1,'Marker','d','MarkerSize',ms)
%males with pentagrams
index1=find(model.GENDER==0);%males
highlight(h,index1,'Marker','p','MarkerSize',ms)

%infection condition

%non-amr
index1=find(model.indiv_state(:,1)==1);
highlight(h,index1,'NodeColor','g')

%amr
index1=find(model.indiv_state(:,2)==1);
highlight(h,index1,'NodeColor','r')

%coinfection
index1=find(sum(model.indiv_state,2)==2);
highlight(h,index1,'NodeColor','cyan')


