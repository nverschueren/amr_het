%test to fill the matrix+. This is th heterosexual case which
%shuffles the stubs for men and women until it's satisfied.

alphaf=3.3;
alpham=2.5;
kmaxm=60;
kmaxf=150;
N=1e3;

Af=(1-alphaf)/(2-alphaf)*(kmaxf.^(2-alphaf)-1)/(kmaxf.^(1-alphaf)-1);
Am=(1-alpham)/(2-alpham)*(kmaxm.^(2-alpham)-1)/(kmaxm.^(1-alpham)-1);


Nfm=Am/Af;%This is Nf/Nm. Since N=Nf+Nm, then  Nm=N/(1+Nfm)
Nm=fix(N/(1+Nfm)); Nf=N-Nm;
%[Nm Nf ]

%ii)
%The node distributions for each group are
ndf =fix( ( ( kmaxf^(-alphaf+1) - 1 ).*rand(Nf,1) + 1 ).^( 1/(-alphaf+1) ));
ndm =fix( ( ( kmaxm^(-alpham+1) - 1 ).*rand(Nm,1) + 1 ).^( 1/(-alpham+1) ));


%iii)
%We make sure  that both groups have the same number of links by
%eliminating one link randomly
while sum(ndf)~=sum(ndm) %  Maybe I can do this all at once.
    
    if (sum(ndf)-sum(ndm))>0 %there are more connections in the female group.
        ind=randi(Nf);
        if(ndf(ind)>1)
            ndf(ind)=ndf(ind)-1;
        end
    else
        ind=randi(Nm);
        if(ndm(ind)>1)
            ndm(ind)=ndm(ind)-1;
        end
    end
end


%iv)
%Once we have both nodes_distributions, we build a matrix with the minimal
%information where the rows are the males and the columns the females.
amf=logical(sparse(Nm,Nf));
%in order to fill the matrix, we construct the list with stubs for females
%and males.

if 0
%small-size example
Nm=3; Nf=5;
amf=zeros(Nm,Nf);
ndm=[1 1 3];
ndf=[1 3 2 4 1];
amf(1,:)=[1 0 0 0 0]';
mask=amf==0;
af=sum(mask);
am=sum(mask,2);

%if there are more ones than zeros, we are stuck
%is not possible to carry on (like being really stuck), when is
%always more ones than zeros 
%so you can carry on, as long as the number of ones is smaller or
%equal to the number of zeros
ndf<=af

ndm'<=am

%Hence, the condition is...

Nm=3;
Nf=5;
amf=zeros(Nm,Nf);
ndm=[1 1 3];
ndf=[1 1 1 1 1];

%amf(1,:)=[0 0 0 0 0]';
end
gen=true;
that=1;
cond=true;
cont=1;
while cond
    
    if gen
        
        mask=amf==0;
        af=full(sum(mask));
        am=full(sum(mask,2));
        
        if ndm(that)==0
            
            zer=find(ndm>0);
            
            if isempty(zer)
                cond=false;
                break;
            end
            this=ndm(zer(randperm(length(zer),1)));
        end
        
        zer=intersect(find(ndf>0),find(amf(this,:)==0));
        if isempty(zer)
            cond=false;
            break;
        end
        that=ndf(zer(randperm(length(zer),1)));
        
        amf(this,that)=1; 
        ndm(this)=ndm(this)-1;
        ndf(that)=ndf(that)-1;
        
        if ndf(that)>0
            this=that;
        end
            
            
        
        gen=~gen;
    else
        
        mask=amf==0;
        af=full(sum(mask));
        am=full(sum(mask,2));
        
        if ndf(that)==0
            
            zer=find(ndf>0);
            
            if isempty(zer)
                cond=false;
                break;
            end
            this=ndf(zer(randperm(length(zer),1)));
        end
        
        zer=intersect(find(ndm>0),find(amf(:,this)==0));
        if isempty(zer)
            cond=false;
            break;
        end
        that=ndm(zer(randperm(length(zer),1)));
        
        amf(that,this)=1; 
        ndm(that)=ndm(that)-1;
        ndf(this)=ndf(this)-1;
        
        if ndm(that)>0
            this=that;
        end
        
        gen=~gen;    
    end
    cond=(any(ndf<=af') || any(ndm<=am));
    cont 
    cont=cont+1;
end