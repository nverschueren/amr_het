%% Simple script to make a video of the evolution of the largest connected component.
% written by NV.
% In order to make this work, you need to uncomment the lines related with the video, before the
% while today<self.n_days in simulate
clear all;
N=3e3; %total population. This is N=Nf+Nm
VERBOSE=false;
%LOW_MEMORY=false;% For some reason it works differently for false
LOW_MEMORY=true;
load('params_het2.mat','params');
params.p0 = [0.5 0.2 0.1];
params.ALLOW_TREAT=false; %Investigating what happens
model=AMR_IBM(N,params,[],VERBOSE,LOW_MEMORY);

n_Days=30;
model.simulate(n_Days);

%%
%
% to do:
% 1) optimise the estroboscopic variable
% 2) put the information in the right place (i.e. the today thing)/
% 3) Include two new properties( maybe logical, one with for showing
% another for recording).









