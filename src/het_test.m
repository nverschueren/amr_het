%% Simple script to test the heterosexual version
% written by NV.
% First, the number of nodes and some regime(logical) variables are assigned
clear all; clc; close all;
N=5e3; %total population. This is N=Nf+Nm
VERBOSE=false; LOW_MEMORY=true;
load('params_het.mat','params');
params.eta=0.2; %test not real
params.p0 = [0.5 0.2 0.1];
params.ALLOW_TREAT=false; %Investigating what happens
% Initialise the model (call the constructor)
model=AMR_IBM(N,params,[],VERBOSE,LOW_MEMORY);
model.MU=0;
model.R=0;
%and simulate
%n_Days=1825;
n_Days=30;
%model.simulate(n_Days); 

%figure(1)=model.plot_net_graph(model,model.adj_full);
%figure(2)=model.plot_lcc(model,model.adj_full);




   



%% Outputs.
% The matrices
%subplot(2,2,1); spy(model.adj_full); subplot(2,2,2); spy(model.adj_int); subplot(2,2,3); spy(model.adj)



%% output (copypasted from the MSM case)

   %% extract all counter data from model object 
    % (or can be referenced directly)
%            data = model.counters

        % plot prevalence time-series for whole simulation
        % (using my built in function)
%             model.plot_prev(data, [0 n_Days], [])
        
%         % manual plots (examples)
%             % get prevalence (per strain) from counter variable
           % (not yet normalised with respect to the population size)
%                 prev_data = 100*data.prevalence./N
%                 
%                 figure('name','Strain prevalence');
%                     hold on;
%                     plot([0:n_Days],prev_data(:,1),'b-'); % non AMR strain
%                     plot([0:n_Days],prev_data(:,2),'r-'); % AMR strain
%                     legend('non-AMR','AMR');
%                     xlabel('Time (days)')
%                     ylabel('Prevalence (%)');
%                     box on;
%                     grid on;
%             
%             % drug administration of each drug given by the cumulative sum
%             % of the daily dosage of each drug
%                 figure('name','Dosage','color','w');
%                     hold on;
%                     plot([0:n_Days], cumsum(data.cipr),'b-');
%                     plot([0:n_Days], cumsum(data.cefta),'r-');
%                     legend('Cipr/A','Ceft/A','location','northwest');
%                     xlabel('Time (days)');
%                     ylabel('Number of doses')
%                     title('Cumulative drug doses administered');
%                     box on;
%                     grid on;
%                     
%     %% Change some parameters and continue simulation
%     
%         % at any point, we can change model parameters by directly
%         % accessing the properties of the model object (this sort of thing
%         % isn't usually adviced in object oriented programming - but we are
%         % scientists, not developers!)
%         
%             % change diagnostics and treatment to discriminatory POC tests
%                 model.ENABLE_POCT = true;
%                 model.DISCRIM_POCT = true;
%             
%             % continue simulation for another n_Days
%                 model.simulate(n_Days);
%                 
%             %% re-plot the cumulative dosages
%             % this time, by accessing the data directly from the counter
%             % variable in the model object. Can also get the last day of simluation
%             % from the value in <object>.today
%                 figure('name','Dosage','color','w');
%                     hold on;
%                     plot([0:model.today], cumsum(model.counters.cipr),'b-');
%                     plot([0:model.today], cumsum(model.counters.cefta),'r-');
%                     ym = get(gca,'ylim');
%                     plot([n_Days n_Days],[ym(1) ym(2)],'k--');
%                     legend('Cipr/A','Ceft/A','location','northwest');
%                     text(n_Days-20, ym(2)/4,'pre-POCT (100% Ceft/A)','rotation',90,'color',[.4 .4 .4]);
%                     text(n_Days+15, ym(2)/3,'POCT introduced','rotation',90,'fontweight','bold');
%                     xlabel('Time (days)');
%                     ylabel('Number of doses')
%                     title('Cumulative drug doses administered');
%                     box on;
%                     grid on;
%             
% 
%     %% extracting numerical data directly from model
%     
%     % Final prevalence of each strain (%): [nonAMR, AMR]
%     % (note here we check the last (end) value of the 3rd dimension of the
%     % state matrix - giving the last time entry)
%         prev = 100*sum(model.indiv_state(:,:,end),1)./N
% 
% %% What's next?
% % 
% % * Reescribir la doc para birthdeath y recovery
% % * Entender y escribir la version para screening y seek_treatment
% %  * Cuales son los escenarios que estan descritos en el paper? como pueden
% %  caracterizarse en terminos de las variables.
% % * Que es burn_in cycle?
% 
% 
%   
%   
% 
