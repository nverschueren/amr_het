%% randomly infecting males....
% In general lines, this script.
%%
% 1) Creates the model using the constructor.
% 2) Set all the infections to zero.
% 3) pick up all the males.
% 4) with a rate given by a new parameter eta, infect some males
% with non-amr
% 5) keep the proportions amr,non-amr, coinfected

% 1) creates the model using the constructor.
clear all; clc; close all;
N=5e3; %total population. This is N=Nf+Nm
VERBOSE=true; LOW_MEMORY=true;
load('params_het.mat','params');
params.p0 = [0.2 0.1 0.1];
params.eta=0.01;
params.ALLOW_TREAT=false; %Investigating what happens
% Initialise the model (call the constructor)
model=AMR_IBM(N,params,[],VERBOSE,LOW_MEMORY);

%2) set the infections to zero (for testing purposes). In practice
%shall we consider non-infected individuals or randomly anybody?
model.indiv_state(:,:)=0;

%3)
malesidx=find(model.GENDER==0);

%4)
eta=20/100; %10 percent
chosen=rand(model.Nm,1)< eta;

malesidx=malesidx(chosen);
model.indiv_state(malesidx,1)=1;

%%Inspired by the initial infection in the constructor.
% for everyone in malexidx infected with nonAMR gonorrhea at this point, 
% switch a fraction p0(2) of those to be AMR infected instead

idx=rand(length(malesidx),1)<model.p0(2);% Elige la mitad de

subamr=malesidx(idx);
model.indiv_state(subamr,1)=0; 
model.indiv_state(subamr,2)=1; 
% test
model.p0(3)=0.8;
%[length(malesidx) sum(model.indiv_state)]
if model.ALLOW_COINFECTION == false && model.p0(3)~=0
    model.p0(3) = 0;
    warning('ALLOW_COINFECTION = false, setting p0(3) = 0');
end
% take a fraction of the amr and reinfect it with non-amr
idx=rand(length(subamr),1)<model.p0(3);
subamr=subamr(idx);
model.indiv_state(subamr,1)=1;

