%% In this program, we consider the heterosexual version
% written by NV.
% Considering the "whole population" N=3e3, we construct the network.
% Instead of evolving the system, we reduce our attention to the the
% largest connected component. We pass this subnetwork to the system as
% partnership network and then we will evolve that new network on time.
clear all; clc; close all;
N=1e3; %total population. This is N=Nf+Nm
VERBOSE=true;
%LOW_MEMORY=true;
LOW_MEMORY=false;
load('params_het2.mat','params');
params.p0 = [0.5 0.2 0.1];
params.ALLOW_TREAT=true; %Investigating what happens

% Initialise the model (call the constructor)

model=AMR_IBM(N,params,[],VERBOSE,LOW_MEMORY);


%model.eta=1/1000;%introducing some gonorrhea.
g=graph(model.adj_full); cc=conncomp(g); indices=find(cc==mode(cc));
%Now, we redefine the size
lcomp=model.adj_full(indices,indices);
gen=model.GENDER(indices);
adj_set.adj=lcomp;
adj_set.alpha_out=model.ALPHA;
adj_set.d_max=model.FULL_MAX_PARTNERS;
adj_set.L=0;
adj_set.x_min=0;
%lets build rel_list
lcomp=tril(lcomp);
rel_list=[];
%rel_list = [rel_list;[m f]];
males=find(gen==0);

for ma=1:length(males)
    
    f=find(lcomp(:,ma)==1);
    m=zeros(length(f),1);
    m(:)=ma;
    rel_list=[rel_list;[m f]];
end
[length(rel_list)   length(unique(rel_list,'rows'))]
adj_set.rel_list=rel_list;                         
                        
 N=length(gen);                       
 Nf=sum(gen);
 Nm=N-Nf;
 
 
 %definition of the subsystem
 model=AMR_IBM(N,params,adj_set,VERBOSE,LOW_MEMORY);
 model.GENDER=gen;
 model.Nm=Nm; model.Nf=Nf;
 model.eta=1/1000;
 
 

 %model.plot_net_graph(model,model.adj_full);
 % n_days=365;

 % model.simulate(2*n_days);

 % model.plot_net_graph(model,model.adj_full); 


 %figure; plot(model.counters.prevalence(:,:));
 %model.plot_lcc(model,model.adj_full);

 





