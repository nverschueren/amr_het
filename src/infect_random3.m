%% Randomly infecting males....
% in general lines, this script (which will be a function in the
% near future)
clear all; clc; close all;
%%
%
% 1) Creates the objet model, using the constructor
% 2) Regardless the regime of memory, obtain the current state in
% 3) Pick up all the male individuals
% 4) Choose a fraction eta of them to be infected 
% terms of the infection (the last 2-column vector in indiv_state)
N=3e3; %N=Nf+Nm
VERBOSE=true;
LOW_MEMORY=false;
%LOW_MEMORY=true;
load('params_het.mat','params');
params.p0=[0.2 0.1 0.1]; % p0(1)= overall prevalence,
                         % p0(2)=percentage of AMR (considering the
                         % infected), p(3)=percentage of coinfected
params.eta=1/100;

model=AMR_IBM(N,params,[],VERBOSE,LOW_MEMORY);
% 2) simulate for 'n_days' and get the infectious' state
n_days=70;
model.simulate(n_days);
if model.LOW_MEM
    current_state = model.indiv_state;
else                   
    current_state = model.indiv_state(:,:,model.today);
end

disp('before')
sum(current_state)
% just to test, let's cure everyone
%current_state(:,:)=0;
%3)
malesidx=find(model.GENDER==0);%find all the males

model.eta=50/100; %the fraction of males to be infected.

chosen=rand(model.Nm,1)<model.eta; %those to be infected

malesidx=malesidx(chosen);

current_state(malesidx,1)=1; %only non-amr for the moment

%Quick check

men=find(model.GENDER==0); 
women=find(model.GENDER==1);

disp('How many individuals are infected with AMR (must be zero)?')
sum(current_state(:,2))

disp('How many individuals are infected with ¬AMR (must be non-zero)?')
sum(current_state(:,1))

disp('How many women are infected with ¬AMR (must be zero)?')
sum(current_state(women,1))

disp('How many men are infected with ¬AMR (must be non-zero)?')
sum(current_state(men,1))
disp('Fraction of infected men vs eta?')
[sum(current_state(men,1))/model.Nm   model.eta]

disp(['<----------NOW WE REINFECT WITH AMR A FRACTION FOLLOWING THE ' ...
       'INITIAL PREVAENCES IN THE CONSTRUCTOR----->'])


%%Inspired by the initial infection in the constructor.
% for everyone infected with nonAMR gonorrhea at this
% point,
%switch a fraction p0(2) of those to be AMR infected instead
model.p0(2)=0.5; %50% each strain

idx=rand(length(malesidx),1)<model.p0(2);

subamr=malesidx(idx);
current_state(subamr,1)=0; 
current_state(subamr,2)=1; 
%check
disp('fraction of infected individuals in with each strain vs p0(2)')
[sum(current_state)/sum(sum(current_state))  model.p0(2)]
disp('again, fraction of infected (regadless the strain) vs eta')
[sum(sum(current_state))/model.Nm  model.eta]





if model.ALLOW_COINFECTION == false && model.p0(3)~=0
    model.p0(3) = 0;
    warning('ALLOW_COINFECTION = false, setting p0(3) = 0');
end

% Finally, take a fraction of the amr and reinfect  them it with non-amr
idx=rand(length(subamr),1)<model.p0(3);
subamr=subamr(idx);
current_state(subamr,1)=1;

%check
disp(['number of individuals co-infected divided by the total ' ...
         'number of individuals with amr vs p0(3)'])

[sum(current_state(:,1).*current_state(:,2))/sum(current_state(:,2)) ...
 model.p0(3)]



%after, the update 
if model.LOW_MEM
    model.indiv_state = current_state;
    
else
    model.indiv_state(:,:,model.today) = current_state;
end


disp('after')
sum(model.indiv_state(:,:,model.today))



