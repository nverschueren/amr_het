%test to fill the matrix+. This is th heterosexual case which
%shuffles the stubs for men and women until it's satisfied.
clear all;close all;clc;
alphaf=3.3;
alpham=2.5;
kmaxm=60;
kmaxf=150;
N=3e3;

Af=(1-alphaf)/(2-alphaf)*(kmaxf.^(2-alphaf)-1)/(kmaxf.^(1-alphaf)-1);
Am=(1-alpham)/(2-alpham)*(kmaxm.^(2-alpham)-1)/(kmaxm.^(1-alpham)-1);


Nfm=Am/Af;%This is Nf/Nm. Since N=Nf+Nm, then  Nm=N/(1+Nfm)
Nm=fix(N/(1+Nfm)); Nf=N-Nm;
%[Nm Nf ]

%ii)
%The node distributions for each group are
ndf =fix( ( ( kmaxf^(-alphaf+1) - 1 ).*rand(Nf,1) + 1 ).^( 1/(-alphaf+1) ));
ndm =fix( ( ( kmaxm^(-alpham+1) - 1 ).*rand(Nm,1) + 1 ).^( 1/(-alpham+1) ));


%iii)
%We make sure  that both groups have the same number of links by
%eliminating one link randomly
while sum(ndf)~=sum(ndm) %  Maybe I can do this all at once.
    
    if (sum(ndf)-sum(ndm))>0 %there are more connections in the female group.
        ind=randi(Nf);
        if(ndf(ind)>1)
            ndf(ind)=ndf(ind)-1;
        end
    else
        ind=randi(Nm);
        if(ndm(ind)>1)
            ndm(ind)=ndm(ind)-1;
        end
    end
end



amf=logical(zeros(Nm,Nf));
tot=sum(ndm)+sum(ndf);
cond=true;
in_chain=false;
while cond
    % m->f
    if in_chain
        this=that;
    else
        nz=find(ndm>0);
        if isempty(nz)
            disp('no more available')
            break
        end
        
        this=nz(randperm(length(nz),1));
    end
    
    nz=find(ndf>0);
    if isempty(nz)
        disp('no more available')
        break
    end
    nz=intersect(nz,find(amf(this,:)==0));
    
    if isempty(nz)
        disp('no more available')
        break
    end
    that=nz(randperm(length(nz),1));
    
    amf(this,that)=1;
    
    ndm(this)=ndm(this)-1;
    ndf(that)=ndf(that)-1;
    
    in_chain=true;
    if ndf(that)==0
        in_chain=false;
    end
    
    
   % f->m
   if in_chain
       this=that;
   else
       nz=find(ndf>0);
       if isempty(nz)
           disp('no more available')
           break
       end
       
       this=nz(randperm(length(nz),1));
   end
       
   nz=find(ndm>0);
   if isempty(nz)
           disp('no more available')
           break
       end
   nz=intersect(nz,find(amf(:,this)==0));
    
   if isempty(nz)
           disp('no more available')
           break
       end
    that=nz(randperm(length(nz),1));
    
    amf(that,this)=1;
    
    ndm(that)=ndm(that)-1;
    ndf(this)=ndf(this)-1;
    
    in_chain=true;
    if ndm(that)==0
        in_chain=false;
    end
    
    
    
    
    mask=amf==0;
    af=full(sum(mask));am=full(sum(mask,2));
    nzndf=find(ndf>0);nzmdf=find(ndm>0);
    
    cond=(any(ndf(nzndf)<=af(nzndf)') && any(ndm(nzmdf)<=am(nzmdf)) && ~isempty(nzndf) && ~isempty(nzmdf));
    [sum(ndm) sum(ndf) length(find(ndf<0)) length(find(ndm<0)) (sum(ndf)+sum(ndm))/tot  ]
end
    
adj=logical(sparse(N,N)); %using the convention 1....Nf,1....Nm
adj(1:Nm,Nm+1:N)=amf;
adj=adj+adj';
figure
spy(adj)
g=graph(adj);
figure
plot(g,'Layout','force')

load('params_het2.mat','params');
model=AMR_IBM(N,params,[],0,1);

