%% Simple script to test the heterosexual version
% written by NV.
% First, the number of nodes and some regime(logical) variables are assigned
clear all; clc; close all;
N=1e4; %total population. This is N=Nf+Nm
VERBOSE=true; LOW_MEMORY=true;
load('params_het2.mat','params');
params.p0 = [0.2 0.1 0];
params.ALLOW_TREAT=false; %Investigating what happens
% Initialise the model (call the constructor)
model=AMR_IBM(N,params,[],VERBOSE,LOW_MEMORY);
