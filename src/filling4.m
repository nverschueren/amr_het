%test to fill the matrix+. This is th heterosexual case which
%shuffles the stubs for men and women until it's satisfied.

alphaf=3.3;
alpham=2.5;
kmaxm=60;
kmaxf=150;
N=8e2;

Af=(1-alphaf)/(2-alphaf)*(kmaxf.^(2-alphaf)-1)/(kmaxf.^(1-alphaf)-1);
Am=(1-alpham)/(2-alpham)*(kmaxm.^(2-alpham)-1)/(kmaxm.^(1-alpham)-1);


Nfm=Am/Af;%This is Nf/Nm. Since N=Nf+Nm, then  Nm=N/(1+Nfm)
Nm=fix(N/(1+Nfm)); Nf=N-Nm;
%[Nm Nf ]

%ii)
%The node distributions for each group are
ndf =fix( ( ( kmaxf^(-alphaf+1) - 1 ).*rand(Nf,1) + 1 ).^( 1/(-alphaf+1) ));
ndm =fix( ( ( kmaxm^(-alpham+1) - 1 ).*rand(Nm,1) + 1 ).^( 1/(-alpham+1) ));


%iii)
%We make sure  that both groups have the same number of links by
%eliminating one link randomly
while sum(ndf)~=sum(ndm) %  Maybe I can do this all at once.
    
    if (sum(ndf)-sum(ndm))>0 %there are more connections in the female group.
        ind=randi(Nf);
        if(ndf(ind)>1)
            ndf(ind)=ndf(ind)-1;
        end
    else
        ind=randi(Nm);
        if(ndm(ind)>1)
            ndm(ind)=ndm(ind)-1;
        end
    end
end



amf=logical(sparse(Nm,Nf));



