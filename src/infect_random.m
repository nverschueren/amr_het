%% randomly infecting males....
% In general lines, this script.
%%
% 1) Creates the model using the constructor.
% 2) Set all the infections to zero.
% 3) pick up all the males.
% 4) with a rate given by a new parameter eta, infect some males
% with non-amr
% 5) keep the proportions amr,non-amr, coinfected

% 1) creates the model using the constructor.
clear all; clc; close all;
N=5e3; %total population. This is N=Nf+Nm
VERBOSE=true; LOW_MEMORY=true;
load('params_het.mat','params');
params.p0 = [0.2 0.1 0.1];
params.ALLOW_TREAT=false; %Investigating what happens
% Initialise the model (call the constructor)
model=AMR_IBM(N,params,[],VERBOSE,LOW_MEMORY);

%2) set the infections to zero (for testing purposes). In practice
%shall we consider non-infected individuals or randomly anybody?
model.indiv_state(:,:)=0;

%3)
malesidx=find(model.GENDER==0);

%4)
eta=50/100; %10 percent.
chosen=rand(model.Nm,1)< eta;

malesidx=malesidx(chosen);

length(malesidx)

%Now we infected them with non-amr
model.indiv_state(malesidx,1)=1;

[sum(sum(model.indiv_state))/model.Nm length(malesidx)]

%%Inspired by the initial infection in the constructor.
% for everyone infected with nonAMR gonorrhea at this point, 
% switch a fraction p0(2) of those to be AMR infected instead
idx=rand(length(malesidx),1)<model.p0(2);% Elige la mitad de

malesidx=malesidx(idx);
%los infectados
model.indiv_state(malesidx,1)=0; 
model.indiv_state(malesidx,2)=1; 
% test
sum(model.indiv_state)
sum(model.indiv_state)/model.Nm
sum(sum(model.indiv_state))/model.Nm



%idx=randperm(size(malesidx,2),round(model.p0(2)*size(malesidx,2)));
%idx=randperm(size(malesidx,2),round(0.5*size(malesidx,2)));% test
                                                           % fifty  fifty
                                                           %idx
                                                           %model.indiv_state(malesidx(idx),2) = 1;
                                                           %model.indiv_state(malesidx(idx),1) = 0;
%% Test

%sum(model.indiv_state)/length(malesidx)



% here take a proportion p0(3) of the AMR (only) infections
% and (re)infect with nonAMR giving the prescribed
% coinfection (given AMR)  proportion
%if model.ALLOW_COINFECTION == false && model.p0(3)~=0
%    model.p0(3) = 0;
%    warning('ALLOW_COINFECTION = false, setting p0(3) = 0');
%end

%id_AMR = find(model.indiv_state(:,2));
%idx = randperm(size(id_AMR,1),round(model.p0(3)*size(id_AMR,1)));
%model.indiv_state(id_AMR(idx),1) = 1;

%% let us see if it makes sense.

%sum(model.indiv_state(:,:))
%sum(sum(model.indiv_state(:,:)))/model.Nm