%% In this program, we consider the heterosexual version
% written by NV.
% Considering the "whole population" N=3e3, we construct the network.
% Instead of evolving the system, we reduce our attention to the the
% largest connected component. We pass this subnetwork to the system as
% partnership network and then we will evolve that new network on time.
clear all; clc; close all;

 N=2e3; %total population. This is N=Nf+Nm
 VERBOSE=true;
%LOW_MEMORY=true;
 LOW_MEMORY=false;
 load('params_het2.mat','params');
 params.p0 = [0.5 0.2 0.1];
 params.ALLOW_TREAT=true; %Investigating what happens
%a single comment

% Initialise the model (call the constructor)

 model=AMR_IBM(N,params,[],VERBOSE,LOW_MEMORY);
[ model.N model.Nm model.Nf];



%model.eta=1/1000;%introducing some gonorrhea.
 g=graph(model.adj_full); cc=conncomp(g); indices=find(cc==mode(cc));
%Now, we redefine the size
 lcomp=model.adj_full(indices,indices);
 



 
 
load msmlcc.mat %the msm largest connected component
                %load lcomp.mat
                %load gen.mat

gen=model.GENDER(indices);

Nhm=length(find(gen==0)); %heterosexual males
Nhf=length(find(gen==1)); %heterosexual females


 Ngm=length(msm_lcc);%number of homosexual men in the network



 adj_set.alpha_out=2.2;
 adj_set.d_max=120;
 adj_set.L=0;
 adj_set.x_min=0;

 adj_set.adj=lcomp;
 tot=blkdiag(lcomp,msm_lcc);
 
 
%% males randomly bysexual
 
 bi=1/1e4; 
 
 bima=rand(Ngm,Nhm)<bi;
  tot(Nhm+Nhf+1:end,1:Nhm)=bima;
  tot(1:Nhm,Nhm+Nhf+1:end)=bima';
 
  %spy(tot);
%figure;
%g=graph(tot); plot(g,'Layout','force')

%before this point we should put some ones accounting for
%interaction between MSM and het Men



adj_set.adj=tot;

tot=tril(tot);
%spy(tot)

    %spy(tot)
rel_list=[];

for ma=1 : length(tot) %eachrow
    f=find(tot(:,ma)==1);
    m=zeros(length(f),1);
    m(:)=ma;
    rel_list=[rel_list;[m f]];
end 

 [length(rel_list)   length(unique(rel_list,'rows'))];

 adj_set.rel_list=rel_list;                         
                        
 ggen=zeros(length(tot),1);
 ggen(1:length(gen))=gen;
 
 load('params_het2.mat','params');
 params.p0 = [0.5 0.2 0.1];
 N=length(tot);
 Nf=sum(ggen);
 Nm=N-Nf;
 
 disp('end of matrices, lets define the model') 
 %Now we start the model
 model=AMR_IBM(N,params,adj_set,1,1);
 model.eta=0;
 model.GENDER=ggen;
 model.Nm=Nm; model.Nf=Nf;
 model.BETA=4*[1 1]*0.001;

 one_year=365;
 model.simulate(10*one_year);
 plot(model.counters.prevalence(:,1)/model.N*100,'o-');
 hold on;
 plot(model.counters.prevalence(:,2)/model.N*100,'*-r'); 
 title('Prevalence in the MSW/MSM pop');
 xlabel('[days]'); ylabel('[fraction]');
 legend('non-AMR','AMR');


 
 
 
  





