%test to fill the matrix+. This is th heterosexual case which
%shuffles the stubs for men and women until it's satisfied.

alphaf=3.3;
alpham=2.5;
kmaxm=60;
kmaxf=150;
N=5e3;

Af=(1-alphaf)/(2-alphaf)*(kmaxf.^(2-alphaf)-1)/(kmaxf.^(1-alphaf)-1);
Am=(1-alpham)/(2-alpham)*(kmaxm.^(2-alpham)-1)/(kmaxm.^(1-alpham)-1);


Nfm=Am/Af;%This is Nf/Nm. Since N=Nf+Nm, then  Nm=N/(1+Nfm)
Nm=fix(N/(1+Nfm)); Nf=N-Nm;
%[Nm Nf ]

%ii)
%The node distributions for each group are
ndf =fix( ( ( kmaxf^(-alphaf+1) - 1 ).*rand(Nf,1) + 1 ).^( 1/(-alphaf+1) ));
ndm =fix( ( ( kmaxm^(-alpham+1) - 1 ).*rand(Nm,1) + 1 ).^( 1/(-alpham+1) ));


%iii)
%We make sure  that both groups have the same number of links by
%eliminating one link randomly
while sum(ndf)~=sum(ndm) %  Maybe I can do this all at once.
    
    if (sum(ndf)-sum(ndm))>0 %there are more connections in the female group.
        ind=randi(Nf);
        if(ndf(ind)>1)
            ndf(ind)=ndf(ind)-1;
        end
    else
        ind=randi(Nm);
        if(ndm(ind)>1)
            ndm(ind)=ndm(ind)-1;
        end
    end
end


%iv)
%Once we have both nodes_distributions, we build a matrix with the minimal
%information where the rows are the males and the columns the females.
amf=logical(sparse(Nm,Nf));
%in order to fill the matrix, we construct the list with stubs for females
%and males.
stubm=zeros(sum(ndm),1); stubf=zeros(sum(ndf),1);
k=0;
for i=1:length(ndm);
    stubm(k+1:ndm(i)+k)=i;
    k=k+ndm(i);
end
k=0;
for i=1:length(ndf);
    stubf(k+1:ndf(i)+k)=i;
    k=k+ndf(i);
end
%stubm contains a number of entries equal to the number of connections in
auxsm=stubm; auxsf=stubf;
stot=zeros(length(auxsm),2);
contador=0;
tic;
while length(stot)~=length(unique(stot,'rows'))
    auxsm=auxsm(randperm(length(auxsm)));
    auxsf=auxsf(randperm(length(auxsf)));
    stot=cat(2,auxsm,auxsf);
    contador=contador+1
end
toc
amf=logical(sparse(Nm,Nf));
ind=sub2ind([Nm,Nf],stot(:,1),stot(:,2));
amf(ind)=1;




adj=logical(sparse(N,N)); %using the convention 1....Nf,1....Nm
adj(1:Nm,Nm+1:N)=amf;
adj=adj+adj';
figure
spy(adj)
g=graph(adj);
figure
plot(g,'Layout','force')



